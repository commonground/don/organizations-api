package server

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/render"

	"gitlab.com/commonground/don/organizations-api/pkg/db"
	"gitlab.com/commonground/don/organizations-api/pkg/xml"
)

const (
	searchParameter = "zoek"
)

type Server struct {
	srv *http.Server
	db  *db.DB
	src *xml.Source
	l   *log.Logger
}

func New(address, basePath string, src *xml.Source) (*Server, error) {
	d, err := db.New()
	if err != nil {
		return nil, err
	}

	s := &Server{
		srv: &http.Server{
			Addr:              address,
			ReadHeaderTimeout: 5 * time.Second,
		},
		db:  d,
		src: src,
		l:   log.Default(),
	}

	r := chi.NewRouter()

	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(render.SetContentType(render.ContentTypeJSON))

	r.Use(noTrailingSlash)
	r.Use(apiVersionHeader)

	r.NotFound(func(w http.ResponseWriter, r *http.Request) {
		_ = render.Render(w, r, notFoundResponse)
	})

	index := func(w http.ResponseWriter, _ *http.Request) { _, _ = w.Write([]byte("Ok\n")) }

	r.Get("/", index)

	if !strings.HasSuffix(basePath, "/") {
		basePath += "/"
	}
	apiBasePath := fmt.Sprintf("%sv%s", basePath, APIMajorVersion)
	log.Printf("API mount path: %s", apiBasePath)

	apiRouter := r.Route(apiBasePath, func(r chi.Router) {
		r.Get("/", index)
		r.Route("/organisaties", func(r chi.Router) {
			r.Get("/", s.listOrganizationsHandler)
			r.Get("/{systemID:[0-9]+}", s.getOrganizationHandler)
		})
	})

	if err := MountOpenAPI(apiRouter, apiBasePath); err != nil {
		return nil, fmt.Errorf("failed to mount OpenAPI specification: %w", err)
	}

	s.srv.Handler = r

	return s, nil
}

func (s *Server) Start() error {
	log.Println("Reading XML source...")

	d, err := xml.Read(s.src.Content)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Done parsing")

	if err := s.db.Load(d); err != nil {
		log.Fatal(err)
	}

	log.Printf("Listening on %s...", s.srv.Addr)
	return s.srv.ListenAndServe()
}

func (s *Server) listOrganizationsHandler(w http.ResponseWriter, r *http.Request) {
	q, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	search := strings.TrimSpace(q.Get(searchParameter))
	orgs, err := s.db.ListOrganizations(search)
	if err != nil {
		s.l.Printf("Failed to get organizations: %v", err)
		render.Status(r, http.StatusInternalServerError)
	}

	render.JSON(w, r, orgs)
}

func (s *Server) getOrganizationHandler(w http.ResponseWriter, r *http.Request) {
	strID := chi.URLParam(r, "systemID")
	if strID == "" {
		render.Status(r, http.StatusBadRequest)
		return
	}

	id, err := strconv.Atoi(strID)
	if err != nil {
		render.Status(r, http.StatusBadRequest)
		return
	}

	o, err := s.db.GetOrganization(id)
	if o == nil && err == nil {
		_ = render.Render(w, r, notFoundResponse)
		return
	} else if err != nil {
		s.l.Printf("Failed to get organization: %v", err)
		render.Status(r, http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, o)
}

var notFoundResponse = &errorResponse{StatusCode: http.StatusNotFound, Message: "Niet gevonden"}

type errorResponse struct {
	StatusCode int    `json:"-"`
	Message    string `json:"melding"`
}

func (e *errorResponse) Render(_ http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.StatusCode)
	return nil
}

func noTrailingSlash(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		pathLen := len(r.URL.Path)
		if pathLen > 1 && r.URL.Path[pathLen-1] == '/' {
			_ = render.Render(w, r, notFoundResponse)
			return
		}

		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

func apiVersionHeader(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("API-Version", APIVersion)
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}
