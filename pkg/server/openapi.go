package server

import (
	_ "embed"
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gopkg.in/yaml.v3"
)

//go:embed static/openapi.yaml
var openAPISpec []byte

type openAPI struct {
	Version    interface{}      `json:"openapi" yaml:"openapi"`
	Info       openAPIInfo      `json:"info" yaml:"info"`
	Servers    []openAPIServers `json:"servers" yaml:"servers"`
	Paths      interface{}      `json:"paths" yaml:"paths"`
	Components interface{}      `json:"components" yaml:"components"`
}

type openAPIInfo struct {
	Title       interface{} `json:"title" yaml:"title"`
	Description interface{} `json:"description" yaml:"description"`
	License     interface{} `json:"license" yaml:"license"`
	Version     string      `json:"version" yaml:"version"`
}
type openAPIServers struct {
	URL string `json:"url" yaml:"url"`
}

func MountOpenAPI(r chi.Router, basePath string) error {
	jsonBytes, yamlBytes, err := BuildOpenAPISpec(basePath)
	if err != nil {
		return err
	}

	r.Get("/openapi.json", func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		_, _ = w.Write(jsonBytes)
	})

	r.Get("/openapi.yaml", func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "application/yaml")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		_, _ = w.Write(yamlBytes)
	})

	return nil
}

func BuildOpenAPISpec(basePath string) (jsonBytes, yamlBytes []byte, err error) {
	o := openAPI{}

	if err = yaml.Unmarshal(openAPISpec, &o); err != nil {
		return
	}

	o.Info.Version = APIVersion
	o.Servers[0].URL = basePath

	jsonBytes, err = json.Marshal(o)
	if err != nil {
		return
	}

	yamlBytes, err = yaml.Marshal(o)
	if err != nil {
		return
	}

	return
}
