package xml

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
)

const (
	sourceFileURL = "https://organisaties.overheid.nl/archive/exportOO.xml"
	minFileSize   = 380
)

type Source struct {
	Content io.Reader
	f       *os.File
}

func NewSource(dirPath string) (*Source, error) {
	filePath := path.Join(dirPath, "exportOO.xml")
	f, err := os.OpenFile(filepath.Clean(filePath), os.O_RDWR|os.O_CREATE, 0o644)
	if err != nil {
		return nil, fmt.Errorf("failed to open source: %w", err)
	}

	s := &Source{
		Content: f,
		f:       f,
	}

	return s, nil
}

func (s *Source) Download() error {
	log.Println("Downloading source file...")

	rsp, err := http.Get(sourceFileURL)
	if err != nil {
		return fmt.Errorf("failed to download file: %w", err)
	}
	defer rsp.Body.Close()

	if rsp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected status: %s", rsp.Status) //nolint:goerr113  // No need to wrap
	}

	if _, err := s.f.Seek(0, io.SeekStart); err != nil {
		return fmt.Errorf("failed to reset file: %w", err)
	}

	size, err := io.Copy(s.f, rsp.Body)
	if err != nil {
		return fmt.Errorf("failed to store file: %w", err)
	}

	if err := s.f.Truncate(size); err != nil {
		return fmt.Errorf("failed to resize file: %w", err)
	}

	if _, err := s.f.Seek(0, io.SeekStart); err != nil {
		return fmt.Errorf("failed to reset file: %w", err)
	}

	return nil
}

func (s *Source) NeedsDownload() bool {
	info, err := s.f.Stat()
	if err != nil {
		return true
	}

	return info.Size() < minFileSize
}
