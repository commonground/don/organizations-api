package xml

import (
	"encoding/xml"
	"io"
)

type Document struct {
	Namespace     string         `xml:"xmlns p,attr"`
	Organizations []Organization `xml:"organisaties>organisatie"`
}

type Organization struct {
	SystemID    int       `xml:"systeemId,attr"`
	Name        string    `xml:"naam"`
	Types       []string  `xml:"types>type"`
	Description *string   `xml:"beschrijving"`
	Addresses   Addresses `xml:"adressen>adres"`
	Contact     *Contact  `xml:"contact"`
}

type Addresses []Address

type Address struct {
	Type        *string `xml:"type" json:"type,omitempty"`
	Street      *string `xml:"straat" json:"straat,omitempty"`
	HouseNumber *string `xml:"huisnummer" json:"huisnummer,omitempty"`
	POBoxNumber *string `xml:"postbus" json:"postbus,omitempty"`
	Postcode    *string `xml:"postcode" json:"postcode,omitempty"`
	Locality    *string `xml:"plaats" json:"plaats,omitempty"`
}

type Contact struct {
	TelephoneNumbers  []ContactTelephoneNumber `xml:"telefoonnummers>telefoonnummer" json:"telefoonnummers"`
	EmailAddresses    []ContactEmailAddress    `xml:"emailadressen>emailadres" json:"emailadressen"`
	InternetAddresses []ContactInternetAddress `xml:"internetadressen>internetadres" json:"internetadressen"`
}

func (c *Contact) Empty() bool {
	if c == nil {
		return true
	}

	return len(c.TelephoneNumbers) == 0 &&
		len(c.EmailAddresses) == 0 &&
		len(c.InternetAddresses) == 0
}

type ContactTelephoneNumber struct {
	Value string  `xml:"nummer" json:"nummer"`
	Label *string `xml:"label" json:"label,omitempty"`
}

type ContactEmailAddress struct {
	Value string  `xml:"email" json:"email"`
	Label *string `xml:"label" json:"label,omitempty"`
}

type ContactInternetAddress struct {
	Value string  `xml:"url" json:"url"`
	Label *string `xml:"label" json:"label,omitempty"`
}

func Read(r io.Reader) (*Document, error) {
	dec := xml.NewDecoder(r)
	doc := &Document{}

	if err := dec.Decode(doc); err != nil {
		return nil, err
	}

	return doc, nil
}

// String is an helper to return a pointer to a string value
func String(v string) *string {
	p := new(string)
	*p = v
	return p
}
