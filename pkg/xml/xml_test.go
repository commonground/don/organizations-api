package xml_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/don/organizations-api/pkg/xml"
)

func TestReadFile(t *testing.T) {
	f, _ := os.Open("testdata/organizations.xml")
	d, err := xml.Read(f)
	require.NoError(t, err)

	assert.Len(t, d.Organizations, 2)

	o := d.Organizations[0]
	assert.Equal(t, 27912852, o.SystemID)
	assert.Equal(t, "Stichting ICTU", o.Name)
	assert.Equal(t, []string{"Organisatie met overheidsbemoeienis"}, o.Types)
	assert.Contains(t, *o.Description, "ICTU is een onafhankelijke advies- en projectenorganisatie")

	o = d.Organizations[1]
	assert.Equal(t, 28110850, o.SystemID)
	assert.Equal(t, "Vereniging van Nederlandse Gemeenten", o.Name)
	assert.Equal(t, []string{"Koepelorganisatie"}, o.Types)
	assert.Contains(t, *o.Description, "dienstverlening vormen de basis van de vereniging.")
}

func TestAddress(t *testing.T) {
	f, _ := os.Open("testdata/address.xml")
	d, err := xml.Read(f)
	require.NoError(t, err)

	t.Run("With addresses", func(t *testing.T) {
		t.Parallel()
		o := d.Organizations[0]
		assert.Len(t, o.Addresses, 2)

		expected := xml.Address{
			Type:        xml.String("Bezoekadres"),
			Street:      xml.String("Nassaulaan"),
			HouseNumber: xml.String("12"),
			Postcode:    xml.String("2514 JS"),
			Locality:    xml.String("Den Haag"),
		}
		assert.Equal(t, expected, o.Addresses[0])

		expected = xml.Address{
			Type:        xml.String("Postadres"),
			POBoxNumber: xml.String("30435"),
			Postcode:    xml.String("2500 GK"),
			Locality:    xml.String("Den Haag"),
		}
		assert.Equal(t, expected, o.Addresses[1])
	})

	t.Run("No addresses", func(t *testing.T) {
		t.Parallel()
		o := d.Organizations[1]
		assert.Nil(t, o.Addresses)
	})
}

func TestContact(t *testing.T) {
	f, _ := os.Open("testdata/contact.xml")
	d, err := xml.Read(f)
	require.NoError(t, err)

	t.Run("With all", func(t *testing.T) {
		t.Parallel()
		c := d.Organizations[0].Contact

		assert.False(t, c.Empty())

		assert.Len(t, c.TelephoneNumbers, 1)
		assert.Equal(t, xml.ContactTelephoneNumber{Value: "01234564789", Label: xml.String("A")}, c.TelephoneNumbers[0])

		assert.Len(t, c.EmailAddresses, 1)
		assert.Equal(t, xml.ContactEmailAddress{Value: "info@example.com", Label: xml.String("B")}, c.EmailAddresses[0])

		assert.Len(t, c.InternetAddresses, 1)
		assert.Equal(t, xml.ContactInternetAddress{Value: "https://example.com", Label: xml.String("C")}, c.InternetAddresses[0])
	})

	t.Run("With multiple email addresses", func(t *testing.T) {
		t.Parallel()
		c := d.Organizations[1].Contact

		assert.False(t, c.Empty())
		assert.Len(t, c.EmailAddresses, 2)
		assert.Equal(t, xml.ContactEmailAddress{Value: "other@example.com"}, c.EmailAddresses[1])
	})

	t.Run("No contact", func(t *testing.T) {
		t.Parallel()

		assert.Nil(t, d.Organizations[2].Contact)
	})

	t.Run("Empty contact", func(t *testing.T) {
		t.Parallel()

		assert.True(t, d.Organizations[2].Contact.Empty())
		assert.True(t, d.Organizations[3].Contact.Empty())
		assert.True(t, d.Organizations[4].Contact.Empty())
	})
}
