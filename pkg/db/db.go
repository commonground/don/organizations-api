package db

import (
	"database/sql"
	"embed"
	"errors"
	"fmt"
	"log"
	"strings"

	"github.com/mattn/go-sqlite3"

	"gitlab.com/commonground/don/organizations-api/pkg/xml"
)

//go:embed sql/*.sql
var sqlFS embed.FS

type DB struct {
	db *sql.DB

	getStatement    *sql.Stmt
	listStatement   *sql.Stmt
	searchStatement *sql.Stmt

	likeReplacer strings.Replacer

	addressesMap map[int]Addresses
	contactMap   map[int]Contact
}

func New() (*DB, error) {
	version, _, _ := sqlite3.Version()
	log.Printf("Using SQLite version: %s", version)
	db, err := sql.Open("sqlite3", "file::memory:?cache=shared")
	if err != nil {
		return nil, err
	}

	// Ensure the dababase is not closed
	db.SetConnMaxIdleTime(0)
	db.SetConnMaxLifetime(0)
	db.SetMaxIdleConns(1)

	schema, _ := sqlFS.ReadFile("sql/schema.sql")

	if _, err = db.Exec(string(schema)); err != nil {
		return nil, fmt.Errorf("failed to create schema: %w", err)
	}

	d := &DB{
		db: db,
	}

	d.getStatement, err = db.Prepare("SELECT system_id, name, types, description FROM organizations WHERE system_id = ?")
	if err != nil {
		return nil, fmt.Errorf("failed to prepeare get statement: %w", err)
	}

	d.listStatement, err = db.Prepare("SELECT system_id, name FROM organizations ORDER BY name")
	if err != nil {
		return nil, fmt.Errorf("failed to prepeare list statement: %w", err)
	}

	d.searchStatement, err = db.Prepare(
		"SELECT system_id, name FROM organizations WHERE name LIKE '%' || ? || '%' ESCAPE '!' ORDER BY name ")
	if err != nil {
		return nil, fmt.Errorf("failed to prepeare search statement: %w", err)
	}

	d.likeReplacer = *strings.NewReplacer(
		"%", "!%",
		"_", "!_",
		"!", "!!",
	)

	d.addressesMap = make(map[int]Addresses)
	d.contactMap = make(map[int]Contact)

	return d, nil
}

func (db *DB) Load(d *xml.Document) error {
	tx, err := db.db.Begin()
	if err != nil {
		return fmt.Errorf("failed to begin transaction: %w", err)
	}

	insert, err := tx.Prepare("INSERT INTO organizations (system_id, name, types, description) VALUES (?, ?, ?, ?)")
	if err != nil {
		return fmt.Errorf("failed to prepare statement: %w", err)
	}

	log.Println("Filling database...")

	for i := range d.Organizations {
		o := d.Organizations[i]
		types := StringArray(o.Types)

		if _, err := insert.Exec(o.SystemID, o.Name, types, o.Description); err != nil {
			return fmt.Errorf("failed to insert record %d: %w", o.SystemID, err)
		}

		if o.Addresses != nil {
			db.addressesMap[o.SystemID] = Addresses(o.Addresses)
		}

		if !o.Contact.Empty() {
			db.contactMap[o.SystemID] = Contact(o.Contact)
		}

	}

	if err = tx.Commit(); err != nil {
		return fmt.Errorf("failed to commit transaction: %w", err)
	}

	_ = insert.Close()

	log.Println("Done")

	return nil
}

type OrganizationItem struct {
	SystemID int    `json:"systeem_id"`
	Name     string `json:"naam"`
}

func (db *DB) ListOrganizations(search string) ([]*OrganizationItem, error) {
	var (
		r   *sql.Rows
		err error
	)

	if search != "" {
		r, err = db.searchStatement.Query(db.likeReplacer.Replace(search))
	} else {
		r, err = db.listStatement.Query()
	}

	if err != nil {
		return nil, err
	}

	defer r.Close()

	orgs := []*OrganizationItem{}

	for r.Next() {
		o := &OrganizationItem{}
		if err := r.Scan(&o.SystemID, &o.Name); err != nil {
			return nil, err
		}
		orgs = append(orgs, o)
	}

	return orgs, nil
}

type OrganizationDetails struct {
	SystemID    int         `json:"systeem_id"`
	Name        string      `json:"naam"`
	Types       StringArray `json:"types"`
	Description *string     `json:"beschrijving"`

	Addresses Addresses `json:"adressen"`
	Contact   Contact   `json:"contact"`
}

type Addresses xml.Addresses

type Contact *xml.Contact

func (db *DB) GetOrganization(systemID int) (*OrganizationDetails, error) {
	r := db.getStatement.QueryRow(systemID)
	o := &OrganizationDetails{}

	if err := r.Scan(&o.SystemID, &o.Name, &o.Types, &o.Description); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, err
		}
		return nil, nil
	}

	o.Addresses = db.addressesMap[systemID]
	o.Contact = db.contactMap[systemID]

	return o, nil
}
