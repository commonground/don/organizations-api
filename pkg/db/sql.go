package db

import (
	"database/sql/driver"
	"errors"
	"strings"
)

const stringArraySep = "|"

var errConvert = errors.New("cannot convert value to StringArray")

type StringArray []string

func (a *StringArray) Scan(src any) error {
	switch src := src.(type) {
	case string:
		if src == "" {
			*a = (*a)[:0]
		} else {
			*a = strings.Split(src, stringArraySep)
		}
		return nil
	case nil:
		*a = nil
		return nil
	}

	return errConvert
}

func (a StringArray) Value() (driver.Value, error) {
	if a == nil {
		return nil, nil
	}

	return strings.Join(a, stringArraySep), nil
}
