package db_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/don/organizations-api/pkg/db"
)

func TestStringArrayScanner(t *testing.T) {
	t.Run("Nil", func(t *testing.T) {
		a := db.StringArray{"value"}
		err := a.Scan(nil)
		assert.NoError(t, err)
		assert.Nil(t, a)
	})

	t.Run("Empty", func(t *testing.T) {
		a := db.StringArray{}
		err := a.Scan("")
		assert.NoError(t, err)
		assert.Equal(t, db.StringArray{}, a)
	})

	t.Run("Single", func(t *testing.T) {
		a := db.StringArray{}
		err := a.Scan("value")
		assert.NoError(t, err)
		assert.Equal(t, db.StringArray{"value"}, a)
	})

	t.Run("Multiple", func(t *testing.T) {
		a := db.StringArray{}
		err := a.Scan("a|b|1")
		assert.NoError(t, err)
		assert.Equal(t, db.StringArray{"a", "b", "1"}, a)
	})

	t.Run("Wrong type", func(t *testing.T) {
		a := db.StringArray{}
		err := a.Scan(true)
		assert.ErrorContains(t, err, "cannot convert value to StringArray")
	})
}
