package db_test

import (
	"fmt"
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/commonground/don/organizations-api/pkg/db"
	"gitlab.com/commonground/don/organizations-api/pkg/xml"
)

func TestOpenDB(t *testing.T) {
	_, err := db.New()
	assert.NoError(t, err)
}

func TestOrganizationsSorting(t *testing.T) {
	doc := xml.Document{
		Organizations: []xml.Organization{
			{SystemID: 1, Name: "XYZ"},
			{SystemID: 2, Name: "abcd"},
			{SystemID: 3, Name: "Cd Ef"},
		},
	}

	d, _ := db.New()
	err := d.Load(&doc)
	require.NoError(t, err)

	orgs, _ := d.ListOrganizations("")

	actual := make([]string, len(orgs))

	for i := range orgs {
		actual[i] = orgs[i].Name
	}

	expected := []string{"abcd", "Cd Ef", "XYZ"}

	assert.Equal(t, expected, actual)
}

func TestOrganizationsSearch(t *testing.T) {
	doc := xml.Document{
		Organizations: []xml.Organization{
			{SystemID: 1, Name: "Geonovum"},
			{SystemID: 2, Name: "Vereniging van Nederlandse Gemeenten"},
			{SystemID: 3, Name: "Vereniging Directeuren Publieksdiensten"},
			{SystemID: 4, Name: "100%"},
			{SystemID: 5, Name: "Werkse!"},
			{SystemID: 6, Name: "liggend _ streepje"},
			{SystemID: 7, Name: "'Stichting Samen Werken'"},
		},
	}

	d, _ := db.New()
	err := d.Load(&doc)
	require.NoError(t, err)

	tests := []struct {
		keyword string
		result  []int
	}{
		{"", []int{1, 2, 3, 4, 5, 6, 7}},
		{" ", []int{2, 3, 6, 7}},
		{"van", []int{2}},
		{"vereniging", []int{2, 3}},
		{"Vereniging", []int{2, 3}},
		{"ten", []int{2, 3}},
		{"TEN", []int{2, 3}},
		{"Geonovum", []int{1}},
		{"G%novum", []int{}},
		{"%", []int{4}},
		{"!", []int{5}},
		{"_", []int{6}},
		{"%%", []int{}},
		{"str__pje", []int{}},
		{"'", []int{7}},
		{";SELECT * FROM organizations", []int{}},
	}

	var ids []int

	for _, tc := range tests {
		t.Run(fmt.Sprintf("keyword_%s", tc.keyword), func(t *testing.T) {
			orgs, _ := d.ListOrganizations(tc.keyword)

			if assert.Len(t, orgs, len(tc.result)) {
				ids = make([]int, len(orgs))

				for i := range orgs {
					ids[i] = orgs[i].SystemID
				}

				sort.Ints(ids)

				assert.Equal(t, tc.result, ids)
			}
		})
	}
}

func TestTypes(t *testing.T) {
	doc := xml.Document{
		Organizations: []xml.Organization{
			{SystemID: 1, Name: "A", Types: nil},
			{SystemID: 2, Name: "B", Types: []string{"Type 1", "Type 2"}},
		},
	}

	d, _ := db.New()
	err := d.Load(&doc)
	require.NoError(t, err)

	t.Run("Nil", func(t *testing.T) {
		t.Parallel()
		o, err := d.GetOrganization(1)
		assert.NoError(t, err)
		assert.Nil(t, o.Types)
	})

	t.Run("Multiple", func(t *testing.T) {
		t.Parallel()
		o, err := d.GetOrganization(2)
		assert.NoError(t, err)
		assert.Len(t, o.Types, 2)
	})
}
