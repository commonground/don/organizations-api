FROM golang:1.21.5-alpine AS build

RUN apk add --no-cache gcc musl-dev
RUN addgroup -S -g 1001 organizations-api && adduser -S -D -H -G organizations-api -u 1001 organizations-api

WORKDIR /go/src/organizations-api

COPY go.mod go.sum ./
COPY vendor vendor

COPY cmd cmd
COPY pkg pkg

RUN go mod verify
RUN go install -tags sqlite_omit_load_extension -v ./cmd/...


FROM alpine:3.19.0

COPY --from=build /go/bin/ /usr/local/bin
COPY --from=build /etc/passwd /etc/group /etc/

USER organizations-api
ENTRYPOINT ["/usr/local/bin/organizations-api"]
