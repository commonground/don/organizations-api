# Organisations API

API for organisations on https://organisaties.overheid.nl/

## How to use the API

All information about the API and how to use it can be found on https://developer.overheid.nl/apis/minbzk-organisaties-overheid

## How it works

Every day https://organisaties.overheid.nl/ publishes an XML with the complete
updated list of government organisations. Every day, this application downloads
that XML file, and updates its database with the latest information on
government organisations. The information of government organisations is then
available through an API.

## Licence

Copyright © VNG Realisatie 2022

[Licensed under the EUPLv1.2](LICENCE.md)
