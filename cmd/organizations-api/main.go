package main

import (
	"flag"
	"log"
	"os"

	"gitlab.com/commonground/don/organizations-api/pkg/server"
	"gitlab.com/commonground/don/organizations-api/pkg/xml"
)

type options struct {
	listenAddress string
	workingDir    string
	apiBasePath   string
}

func parseFlags() *options {
	o := &options{}
	flag.StringVar(&o.listenAddress, "listen-address", envValue("LISTEN_ADDRESS", "127.0.0.1:8080"), "Listen address")
	flag.StringVar(&o.workingDir, "working-dir", envValue("WORKING_DIR", "tmp"), "Path to of a working directory")
	flag.StringVar(&o.apiBasePath, "api-base-path", envValue("API_BASE_PATH", "/"), "Base path for the API endpoint")

	flag.Parse()

	return o
}

func main() {
	o := parseFlags()

	src, err := xml.NewSource(o.workingDir)
	if err != nil {
		log.Fatalf("Creating source: %v", err)
	}

	if src.NeedsDownload() {
		if err := src.Download(); err != nil {
			log.Fatalf("Downloading source: %v", err)
		}
	}

	srv, err := server.New(o.listenAddress, o.apiBasePath, src)
	if err != nil {
		log.Fatalf("Creating server: %v", err)
	}

	log.Println("Starting server...")
	if err := srv.Start(); err != nil {
		log.Fatal(err)
	}
}

func envValue(key, defaultValue string) string {
	value := os.Getenv(key)
	if value == "" {
		return defaultValue
	}
	return value
}
