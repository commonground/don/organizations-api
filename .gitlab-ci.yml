include:
  - project: commonground/don/ci/templates
    file: templates/base.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml

variables:
  SCAN_KUBERNETES_MANIFESTS: "true"


Lint:
  stage: test
  image: golangci/golangci-lint:v1.55.2-alpine
  script:
    - golangci-lint run --out-format code-climate:codequality.json,colored-line-number
  artifacts:
    reports:
      codequality: codequality.json

Test:
  stage: test
  image: golang:1.21.5
  script:
    - go test -coverprofile coverage.out ./...
    - go tool cover -func coverage.out
  coverage: /total:\t+\(statements\)\t+([\d\.]+?%)/

Validate OpenAPI:
  stage: test
  image:
    name: redocly/cli:1.6.0
    entrypoint: [""]
  variables:
    REDOCLY_TELEMETRY: "off"
  script:
    - openapi lint --format codeclimate --lint-config error | tee codequality.json
  artifacts:
    reports:
      codequality: codequality.json

Build:
  extends: .don_build_image

Deploy to review:
  stage: deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    REVIEW_BASE_DOMAIN: nlx.reviews
    HELM_KUBECONTEXT: "commonground/don/ci/kubernetes-agents:review"
    HELM_MAX_HISTORY: "1"
    K8S_NAMESPACE: "don-${CI_ENVIRONMENT_SLUG}"
  before_script:
    - BUILD_IMAGE="`cat ci_build_image.txt`"
    - export IMAGE_REPOSITORY="${BUILD_IMAGE%:*}"
    - export IMAGE_TAG="${BUILD_IMAGE#*:}"
  script:
    - |
      helm upgrade organizations-api ./helm/organizations-api \
        --install --create-namespace \
        --namespace "${K8S_NAMESPACE}" \
        --set-string "image.repository=${IMAGE_REPOSITORY}" \
        --set-string "image.tag=${IMAGE_TAG}" \
        --set-string "ingress.host=${K8S_NAMESPACE}.${REVIEW_BASE_DOMAIN}" \
        --set-string "podAnnotations.app\.commonground\.nl/git-revision=${CI_COMMIT_SHORT_SHA}" \
        --values helm/organizations-api/values-review.yaml
    - echo -n "https://${K8S_NAMESPACE}.${REVIEW_BASE_DOMAIN}" > ci_deploy_url.txt
  environment:
    name: $CI_COMMIT_REF_NAME
    url: https://${K8S_NAMESPACE}.${REVIEW_BASE_DOMAIN}
    on_stop: Remove from review
  artifacts:
    paths:
      - ci_deploy_url.txt
  dependencies:
    - Build
  needs:
    - Build
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^review\/.*$/'

Remove from review:
  stage: deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    GIT_STRATEGY: none
    KUBE_CONTEXT: "commonground/don/ci/kubernetes-agents:review"
    K8S_NAMESPACE: "don-${CI_ENVIRONMENT_SLUG}"
  script:
    - kubectl --context "${KUBE_CONTEXT}" delete namespace "${K8S_NAMESPACE}"
  environment:
    name: $CI_COMMIT_REF_NAME
    action: stop
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^review\/.*$/'
      when: manual
      allow_failure: true

Release:
  extends: .don_release_image
  dependencies:
    - Build
  needs:
    - Build

Deploy to production:
  stage: deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    HELM_KUBECONTEXT: "commonground/don/ci/kubernetes-agents:production"
    HELM_MAX_HISTORY: "2"
    K8S_NAMESPACE: tn-commonground-don
  before_script:
    - RELEASE_IMAGE="`cat ci_release_image.txt`"
    - IMAGE_REPOSITORY="${RELEASE_IMAGE%:*}"
    - IMAGE_TAG="${RELEASE_IMAGE#*:}"
  script:
    - |
      helm upgrade organizations-api ./helm/organizations-api \
        --install \
        --namespace ${K8S_NAMESPACE} \
        --set-string image.repository=${IMAGE_REPOSITORY} \
        --set-string image.tag=${IMAGE_TAG} \
        --values helm/organizations-api/values-production.yaml
  environment:
    name: production
    url: https://developer.overheid.nl/oo/api
  dependencies:
    - Release
  needs:
    - Release
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

Validate ADR:
  stage: integration
  image:
    name: registry.gitlab.com/commonground/don/adr-validator:0.5.0
    entrypoint: [""]
  before_script:
    - export DEPLOY_URL="`cat ci_deploy_url.txt`"
  script:
    - until wget -q --spider "${DEPLOY_URL}"; do sleep 3; done
    - adr-validator validate --details --fail-not-passed core "${DEPLOY_URL}/v0"
  dependencies:
    - Deploy to review
  needs:
    - Deploy to review
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^review\/.*$/'

container_scanning:
  stage: security
  before_script:
    - export CS_IMAGE="`cat ci_build_image.txt`"
  dependencies:
    - Build
  needs:
    - Build

dependency_scanning:
  stage: security
  needs:
    - Test

sast:
  stage: security
  variables:
    SAST_EXCLUDED_ANALYZERS: "flawfinder"
  needs:
    - Test
